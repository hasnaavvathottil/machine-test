<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    protected $fillable = ['user_id','stock_price_id','count'];

}
