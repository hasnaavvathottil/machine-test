<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $fillable = ['name'];
    
    public function details(){
    	return $this->hasMany('App\UomDetail','uom_id');
    }
}
