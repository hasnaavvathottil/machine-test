<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uom;
use App\UomDetail;

class UomController extends Controller
{
    public function create(){
    	return view('uom.create');
    }
    public function store(Request $request){
    	$requestAll = $request->all();
    	$uom = Uom::create(['name'=>$requestAll['name']]);
    	if($uom){
	    	foreach($requestAll['details'] as $item){
	    		$new = UomDetail::create(['name'=>$item['name'],'uom_id'=>$uom->id]);
	    	}
	    }
    	return redirect()->back()->with('success', 'Uom created successfully!');
    }
}
