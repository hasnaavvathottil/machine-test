<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uom;
use App\Product;
use App\StockPrice;
use App\ProductUomMapping;
use App\UserCart;
use Auth;

class ProductController extends Controller
{
    public function create(){
    	$uom =  Uom::with('details')->get();
    	// return $uom;
    	return view('product.create',compact('uom'));
    }

    public function store(Request $request){
    	$requestAll = $request->all();
    	// return $requestAll;
    	$product = Product::create(['name'=>$requestAll['name']]);
    	if($product){
	    	foreach($requestAll['details'] as $item){
	    		$new = StockPrice::create([
	    			'price'=>$item['price'],
	    			'stock'=>$item['stock'],
	    			'product_id'=>$product->id
	    		]);
	    		foreach($item['uom_details'] as $uom){
	    			$newUom = ProductUomMapping::create([
		    			'uom_details_id'=>$uom,
		    			'stock_price_id'=>$new->id,
		    			'product_id'=>$product->id
	    			]);
	    		}
	    	}
	    }

    	return redirect()->back()->with('success', 'Product created successfully!');
    }
    public function index(){
    	$products = Product::with('getStockPrice.getCombinations.getUnit')->get();
        // return $products;
    	return view('product.index',compact('products'));
    }
    public function getPrice(Request $request){
        $id = $request->get('stock_price_id');
        $result = StockPrice::find($id);
        return response()->json(['status' => true, 'data' => $result]);
    }
    public function addCart(Request $request){
        $id = $request->get('stock_price_id');
        $exist = UserCart::where('user_id',Auth::user()->id)
        ->where('stock_price_id',$id)->first();
        if($exist){
            $exist->increment('count');
        }else{
            $result = UserCart::create([
                'user_id'=>Auth::user()->id,
                'stock_price_id'=>$id,
                'count'=>1
            ]);
        }
        
        return response()->json(['status' => true]);
    }
    public function getCart(){

        $cartList = StockPrice::with('product','getCombinationsCart.getUnit')
        ->join('user_carts','stock_price_id','=','stock_prices.id')
        ->where('user_id',Auth::user()->id)->get();
        // return $cartList;
        
        return view('cart.list',compact('cartList'));
    }

}
