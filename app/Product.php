<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['name'];

    public function getStockPrice(){
    	return $this->hasMany('App\StockPrice','product_id')->where('stock','>',0);
    }

   

}
