<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUomMapping extends Model
{
    protected $fillable = ['uom_details_id','product_id','stock_price_id'];

    public function getUnit(){
    	return $this->belongsTo('App\UomDetail','uom_details_id');
    }
}
