<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockPrice extends Model
{
	protected $fillable = ['product_id','stock','price'];
    
     public function getCombinationsCart(){
    	return $this->hasMany('App\ProductUomMapping','stock_price_id','stock_price_id');
    }
    public function getCombinations(){
    	return $this->hasMany('App\ProductUomMapping','stock_price_id');
    }
    public function product(){
    	return $this->belongsTo('App\Product','product_id');
    }
}
