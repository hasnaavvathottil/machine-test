<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UomDetail extends Model
{
    protected $fillable = ['name','uom_id'];
}
