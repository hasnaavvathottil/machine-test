@extends('layouts.partials.out.main')
@section('content')
    <div class="login-box">
        <div class="login-logo">

            <img src="{{url('/images/logo-v.png')}}">

        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form  method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group has-feedback">
                        <label for="email" class=" col-form-label text-md-right">{{ __('E-Mail') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif


                    </div>
                    <div class="form-group has-feedback">
                        <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
                            </div>
                            {{--<div class="checkbox icheck">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                            </div>--}}
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                            {{--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--}}
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                {{--<div class="social-auth-links text-center mb-3">--}}
                    {{--<p>- OR -</p>--}}
                    {{--<a href="#" class="btn btn-block btn-primary">--}}
                        {{--<i class="fa fa-facebook mr-2"></i> Sign in using Facebook--}}
                    {{--</a>--}}
                    {{--<a href="#" class="btn btn-block btn-danger">--}}
                        {{--<i class="fa fa-google-plus mr-2"></i> Sign in using Google+--}}
                    {{--</a>--}}
                {{--</div>--}}
                <p class="mb-1">
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                    @endif
                </p>
                {{--<p class="mb-0">--}}
                    {{--<a href="register.html" class="text-center">Register a new membership</a>--}}
                {{--</p>--}}
            </div>
        </div>
    </div>
@endsection
