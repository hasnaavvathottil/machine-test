@extends('layouts.partials.main')
@section('title','Products')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('/product')}}">Products</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Add Product</h3>
                    </div>
                    <form role="form" method="POST" action="{{url('/product')}}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="inputName">Name</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" id="inputName"
                                               placeholder="Enter Product Name">
                                        @if ($errors->has('name'))
                                            <span
                                                class="text-danger"><strong> * {{ $errors->first('name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-info btn-sm" id="add-row">Add Row</button>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="inputName">Details</label>
                                    </div>
                                    <div class="col-sm-9">
                                       <table>
                                        <tr>
                                            @foreach($uom as $item)
                                            <th>
                                                 {{$item->name}}
                                            </th>
                                            @endforeach
                                            <th>Price</th>
                                            <th>Stock</th>
                                        </tr>
                                           <tbody id="detail-list">
                                           </tbody>
                                       </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </form>
                    <table id="row-field" style="display: none">
                         <tr>
                            @foreach($uom as $item)
                            <td>
                                 <select class="form-control" name="details[replace_i][uom_details][{{$item->id}}]">
                                    @foreach($item->details as $i)
                                    <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach     
                                 </select>
                            </td>
                            @endforeach
                            <td>
                                 <input type="text" class="form-control price" placeholder="Enter Price">
                            </td>
                            <td>
                                 <input type="text" class="form-control stock"  placeholder="Enter Stock">
                            </td>
                            <td><ul class="nav nav-pills text-center" ><li><a href="#"  data-toggle="pill" style="color:red"><i class='fa fa-trash fa-fw remove_field'></i></a></li></ul></td>
                         </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer-script')
    <script>
        $(function () {
            $('.select2').select2();
            var i=1;
            addRow();
            $("#add-row").on('click',function(e){
                 e.preventDefault();
                 
                 addRow();
            });
            function addRow(){
                line = $("#row-field tr").clone();
                 line.find('span').remove();
                 lineHTML = line.html();
                 l = lineHTML.replace(/replace_i/g,i);
                 line1 = line.html(l);
                 line1.find('.stock').attr("name","details["+i+"][stock]");
                 line1.find('.price').attr("name","details["+i+"][price]");
                 line1.find('input').val('');
                 line1.attr('class','extra-line');

                 console.log(line1);
                 line.appendTo('#detail-list');
                 i++;
            }
            $("#detail-list").on('click','.remove_field',function(e){
                e.preventDefault();
                $(this).closest('.extra-line').remove();
            });
        });
    </script>
@endsection
