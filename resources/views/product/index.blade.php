@extends('layouts.partials.main')
@section('title','Products')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Product</h1>
                </div>
                <p>
                    You can view the price of every varients by clicking it eg:Red S, 
                    You can add product in cart by clicking cart button 
                </p>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('/product')}}">Products</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            @foreach($products as $product)
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{$product->name}}</h3>
                    <p>
                    @foreach($product->getStockPrice as $varients)
                        <span class="varient" data-price-id="{{$varients->id}}">
                        @foreach($varients->getCombinations as $v)
                            {{$v->getUnit->name}}
                        @endforeach
                    </span>
                        &nbsp&nbsp&nbsp&nbsp
                        
                    @endforeach
                    </p>

                  </div>
                  <div class="icon">
                    <i class="ion ion-bag"></i>
                  </div>
                  <form class="addToMyCart">
                    <input type="hidden" class="price" name="stock_price_id" value="{{$product->getStockPrice[0]->id}}">
                    @csrf
                  <a href="#" class="small-box-footer">{{$product->getStockPrice[0]->price}}
                    <i class="fa fa-shopping-cart"></i></a>
                </form>
                </div>
              </div>
              @endforeach
        </div>
    </section>
@endsection
@section('footer-script')
    <script>
        $(function () {
            $('.varient').click(function(){
                $('.varient').removeClass('active');
                id = $(this).attr('data-price-id');
                var $t = $(this);
                // $(this).closest('.small-box').find('.small-box-footer').html(100);
                $.get('price',{'stock_price_id':id},function(data){
                    if(data.status){
                    // $(this).addClass('active');
                    $t.closest('.small-box').find('.small-box-footer').html(data.data.price+'<i class="fa fa-shopping-cart"></i>');
                    $t.closest('.small-box').find('.price').val(data.data.id);
                    }
                    console.log(data);
                });
            });
            $('.small-box-footer').click(function(){
                id = $(this).attr('data-price-id');
                form = $(this).closest('.small-box').find('.addToMyCart').serialize();
                $.post('add-cart',form,function(data){
                    if(data.status){
                        alert('successfully added');
                    }
                    console.log(data);
                });
            });
            
        });
    </script>
@endsection
