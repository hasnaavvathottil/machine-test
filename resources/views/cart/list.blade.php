@extends('layouts.partials.main')
@section('title','Products')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cart</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Cart</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table class="table table-bordered">
                      <tbody><tr>
                        <th style="width: 10px">#</th>
                        <th>Product</th>
                        <th></th>
                        <th>Count</th>
                        <th style="width: 40px">Price</th>
                      </tr>
                      @foreach($cartList as $key=>$item)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->product->name}}</td>
                        <td>
                          @foreach($item->getCombinationsCart as $c)
                          {{$c->getUnit->name}}
                          @endforeach
                        </td>
                        <td>{{$item->count}}</td>
                        <td>{{$item->price}}</td>
                      </tr>
                      @endforeach
                     
                    </tbody></table>
                  </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer-script')
    <script>
        $(function () {
            $('.varient').click(function(){
                $('.varient').removeClass('active');
                id = $(this).attr('data-price-id');
                var $t = $(this);
                // $(this).closest('.small-box').find('.small-box-footer').html(100);
                $.get('price',{'stock_price_id':id},function(data){
                    if(data.status){
                    // $(this).addClass('active');
                    $t.closest('.small-box').find('.small-box-footer').html(data.data.price);
                    $t.closest('.small-box').find('.price').value(data.data.id);
                    }
                    console.log(data);
                });
            });
            $('.small-box-footer').click(function(){
                id = $(this).attr('data-price-id');
                form = $(this).closest('.small-box').find('.addToMyCart').serialize();
                $.post('add-cart',form,function(data){
                    if(data.status){
                        alert('successfully added');
                    }
                    console.log(data);
                });
            });
            
        });
    </script>
@endsection
