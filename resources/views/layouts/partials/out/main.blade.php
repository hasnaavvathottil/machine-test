<!DOCTYPE html>
<html>
<head>
    @include('layouts.partials.out.header')
</head>
<body class="hold-transition login-page">

    @yield('content')

{{--
@include('layouts.partials.footer')
--}}
    <script src="{!! asset('theme/plugins/jquery/jquery.min.js') !!}"></script>
    <script src="{!! asset('theme/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

    <!-- Bootstrap 4 -->
    <!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>

    <script>
       /* $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass   : 'iradio_square-blue',
                increaseArea : '20%' // optional
            })
        })*/
    </script>

</body>
</html>
