<!DOCTYPE html>
<html lang="en">
<head>
    <title>GetLead Sales | @yield('title')</title>
    @include('layouts.partials.head')
</head>
<body class="hold-transition sidetleebar-mini">
<div class="wrapper">
    <!-- Navbar -->
@include('layouts.partials.nav')
<!-- /.navbar -->

    <!-- Main Sidebar Container -->
@include('layouts.partials.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('flash-message')
    @yield('content')
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@include('layouts.partials.footer')

<!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.partials.footer-script')
@yield('footer-script')
</body>
</html>
