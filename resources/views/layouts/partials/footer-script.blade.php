<!-- jQuery -->
<script src="{!! asset('theme/plugins/jquery/jquery.min.js') !!}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{!! asset('theme/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
<!-- DataTables -->
<script src="{!! asset('theme/plugins/datatables/jquery.dataTables.js') !!}"></script>
<script src="{!! asset('theme/plugins/datatables/dataTables.bootstrap4.js') !!}"></script>
<!-- SlimScroll -->
<script src="{!! asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<!-- FastClick -->
<script src="{!! asset('theme/plugins/fastclick/fastclick.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('theme/dist/js/adminlte.min.js') !!}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{!! asset('theme/dist/js/demo.js') !!}"></script>
<script src="{!! asset('theme/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('theme/plugins/jquery-confirm/jquery-confirm.min.js') !!}"></script>
<!-- page script -->
