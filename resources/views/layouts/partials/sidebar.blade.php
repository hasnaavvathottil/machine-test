<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link  bg-gray-light">
        {{--<img src="{{url('/images/getlead-logo.svg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-strong">GETLEAD</span>--}}

    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{url('theme/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{Auth::user()->name}} 
                   
                        [ Admin ]
                    
                </a>
            </div>
        </div>
        <nav class="mt-2">
            @include('layouts.partials.menu')
        </nav>
    </div>
</aside>
