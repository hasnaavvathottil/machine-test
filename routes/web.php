<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();


Route::group(['middleware' => ['auth']], function () {

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('uom', 'UomController@create')->name('uom.create');
	Route::post('uom', 'UomController@store')->name('uom.store');
	Route::get('product', 'ProductController@create')->name('product.create');
	Route::post('product', 'ProductController@store')->name('product.store');
	Route::get('product-list', 'ProductController@index')->name('product.index');
	Route::get('price', 'ProductController@getPrice')->name('get.price');
	Route::post('add-cart', 'ProductController@addCart')->name('cart.add');
	Route::get('get-cart', 'ProductController@getCart')->name('cart.get');
});

